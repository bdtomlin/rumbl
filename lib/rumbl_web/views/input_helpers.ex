defmodule RumblWeb.InputHelpers do
  use Phoenix.HTML

  @error_class "input-with-error"

  def input(form, field, opts \\ []) do
    type = opts[:type] || Phoenix.HTML.Form.input_type(form, field)
    opts = add_state_class(form, field, opts)

    input(type, form, field, Keyword.delete(opts, :type))
  end

  defp input(:select, form, field, opts) do
    options = Keyword.get(opts, :options)
    opts = Keyword.delete(opts, :options)

    input = Phoenix.HTML.Form.select form, field, options, opts
    render_input(form, field, input)
  end

  defp input(:multiple_select, form, field, opts) do
    options = Keyword.get(opts, :options)
    opts = Keyword.delete(opts, :options)

    input = Phoenix.HTML.Form.multiple_select form, field, options, opts
    render_input(form, field, input)
  end

  defp input(type, form, field, opts) do
    input = apply(Phoenix.HTML.Form, type, [form, field, opts])
    render_input(form, field, input)
  end

  defp render_input(form, field, input) do
    error = RumblWeb.ErrorHelpers.error_tag(form, field) || ""
    [input, error]
  end

  defp add_state_class(form, field, opts) do
    cond do
      form.errors[field] ->
        Keyword.put(opts, :class, Keyword.get(opts, :class, "") <> " #{@error_class}")
      true -> opts
    end
  end

end